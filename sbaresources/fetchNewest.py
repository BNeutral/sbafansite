#Downloads the latest files recursively. Requeris wget2 as part of the OS path to be called

import requests
import os
import re

LATEST_FILE = "./latest.txt"
DRIPS = "https://static.drips.pw/steambirds/config/"

#Hits the site and returns the string name of the latest config folder, or raises an error upon failure
def getLatestConfigName():
    r = requests.get(DRIPS)
    code = r.status_code
    if code != 200:
        raise Exception("Html error code: "+str(code))
    lines = r.text.split("\n")
    regex = re.compile("^<a href=\"(config_\d+_\d+)/\"")
    matches = []
    for line in lines:
        match = regex.match(line)
        if match:
            matches.append(match.group(1))
    matches.sort(reverse=True)
    return matches[0]

#Checks if we already have the latest config downloaded
def alreadyDownloaded(latestConfig):
    if not os.path.isfile(LATEST_FILE):
        return False
    lFile = open(LATEST_FILE, "r")
    readLine = lFile.readline().strip()
    lFile.close()
    if latestConfig != readLine:
        return False
    if not os.path.isdir(latestConfig):
        return False
    return True

#Downloads the config directory into a folder of the same name
def downloadDir(latest):
    print("Downloading")
    os.system("wget2 --reject=index.html,robots.txt --recursive --no-parent --no-host-directories --level=0 --cut-dirs=2 "+DRIPS+latest+"/")

#Writes the latest config to the latest text file
def updateFile(latest):
    print("Updating file")
    with open(LATEST_FILE, "w") as lFile:
        readLine = lFile.write(latest)

#Checks for updates and gets them if needed
def run():
    latest = getLatestConfigName()
    if not alreadyDownloaded(latest):
        print("Found new config: "+latest)
        downloadDir(latest)
        updateFile(latest)

run()
