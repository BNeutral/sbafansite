package sba.trading

import grails.gorm.transactions.Transactional

@Transactional
class TradeService {

    def notificationService;

    // Selling just one item for gold
    // Pass buyer id of 0 for null
    def Offer newOfferGold (int senderId,int sellerId, int buyerId, String ownedItemId, int gold, FeePayer payer, 
    FeeCurrency currency, int stock, OfferStatus status = OfferStatus.PUBLIC, Offer previous = null) {
        return newOffer(senderId, sellerId, buyerId, [ownedItemId], [1], [], [], gold, payer, currency, stock, status, previous)
    }

    // Selling one or a few of the same item for one or a few of a different item
    def Offer newOfferSimpleTrade (int senderId, int sellerId, int buyerId, String ownedItemId, int ownedCount, 
    String wantedItemId, int wantedCount, int gold, FeePayer payer, FeeCurrency currency, int stock, 
    OfferStatus status = OfferStatus.PUBLIC, Offer previous = null) {
        return newOffer(senderId, sellerId, buyerId, [ownedItemId], [ownedCount], [wantedItemId], [wantedCount], gold, payer, currency, stock, status, previous)
    }

    def Offer newOffer (int senderId, int sellerId, int buyerId, List<String> ownedItemIds, 
    List<Integer> ownedCounts, List<String> wanteditemIds, List<Integer> wantedCounts, int gold, 
    FeePayer payer, FeeCurrency currency, int stock, OfferStatus status = OfferStatus.PUBLIC,
    Offer previous = null) {
        User sender = User.findById(senderId)
        User seller = User.findById(sellerId)
        User buyer = null
        if (status != OfferStatus.PUBLIC) buyer = User.findById(buyerId)
        List<ItemInstance> ownedItems = new ArrayList<ItemInstance>()
        List<ItemInstance> wantedItems = new ArrayList<ItemInstance>()
        for (int i = 0; i < ownedItemIds.size(); ++i) {
            ownedItems.add(new ItemInstance(itemData: ownedItemIds[i], count:ownedCounts[i]) )
        }
        for (int i = 0; i < wanteditemIds.size(); ++i) {
            wantedItems.add(new ItemInstance(itemData: wanteditemIds[i], count:wantedCounts[i]) )
        }
        return newOffer(sender, seller, buyer, ownedItems, wantedItems, gold, payer, currency, stock, status, previous)
    }

    def Offer newOffer (User sender, User seller, User buyer, Price price, int gold, FeePayer payer,
    FeeCurrency currency, int stock, OfferStatus status = OfferStatus.PUBLIC, Offer previous = null) {
        return newOffer(sender, seller, buyer, price.owns, price.wants, gold, payer, currency, stock, status, previous, buyer)
    }

    def Offer newOffer (User sender, User seller, User buyer, List<ItemInstance> ownedItems,
    List<ItemInstance> wantedItems, int gold, FeePayer payer, FeeCurrency currency, int stock,
    OfferStatus status = OfferStatus.PUBLIC, Offer previous = null) {
        Price price = new Price(gold: gold) 
        ownedItems.each {
            price.addToOwns(itemData: it.itemData, count: it.count) 
        }
        wantedItems.each {
            price.addToWants(itemData: it.itemData, count: it.count) 
        }
        Offer offer = new Offer( 
            sender: sender,
            seller: seller, 
            buyer: buyer,
            price: price, 
            status: status, 
            payer: payer,
            currency: currency,
            stock: stock,
            previous: previous
        )
        offer.fee = offer.price.calculateFee()
        offer.save(failOnError: true);
        return offer
    }

    def void remove(Offer offer) {
        offer.delete()
    }

    // Accepts the offer by creating a trade agreement
    def void acceptOffer(int offerId, int accepterId, String message = "") {
        Offer offer = Offer.findById(offerId)
        User accepter = User.findById(accepterId)
        User notSender = offer.getNotSender()
        if ( (notSender != null && accepter != notSender) || offer.stock <= 0 
            || (offer.status != OfferStatus.PUBLIC && offer.status != OfferStatus.NEGOTIATING)) {
            println(accepter)
            println(offer.getNotSender())
            return
        }
        User buyer = offer.buyer
        if (buyer == null && offer.seller != buyer) buyer = accepter
        TradeAgreement agreement = new TradeAgreement(
            sender: accepter,
            seller: offer.seller,
            buyer: buyer,
            offer: offer,
            status: TradeStatus.PENDING)
        .save(failOnError: true)
        offer.reduceStock()
        if (offer.status == OfferStatus.NEGOTIATING) {
            offer.status = OfferStatus.ACCEPTED
        }        
        notificationService.notifyNewAgreement(offer.getNotSender(), agreement)
        offer.save(failOnError: true)
    }

    // Reject a non public offer
    def void rejectOffer(int offerId, int rejecterId, String message = "") {
        Offer offer = Offer.findById(offerId)
        User rejecter = User.findById(rejecterId)
         if (rejecter != offer.getNotSender() || offer.status != OfferStatus.NEGOTIATING) {
            return
        }
        if (offer.status != OfferStatus.PUBLIC) {
            offer.status = OfferStatus.REJECTED
            notificationService.notifyNewAgreement(offer.getNotSender(), agreement)
            offer.save(failOnError: true)
        }
    }

    def Offer newCounterOffer (int senderId, int baseOfferId, List<String> items, List<Integer> quantities, int gold,
    FeePayer payer, FeeCurrency currency) {
        List<ItemInstance> buyerItems = new ArrayList<ItemInstance>()
        for (int i = 0; i < items.size(); ++i) {
            buyerItems.add(new ItemInstance(itemData: items[i], count:quantities[i]) )
        }
        return newCounterOffer(senderId, baseOfferId, buyeritems, gold, payer, currency)
    }

    def Offer newCounterOffer (int senderId, int baseOfferId, List<ItemInstance> buyerItems, int gold,
    FeePayer payer, FeeCurrency currency) {
        Offer baseOffer = Offer.findById(baseOfferId)
        User sender = User.findById(senderId)
        if (sender == baseOffer.seller) return
        return newCounterOffer(sender, baseOffer, buyerItems, gold, payer, currency)
    }

    // Make a new non public offer against another
    def Offer newCounterOffer (User sender, Offer baseOffer, List<ItemInstance> buyerItems, int gold,
    FeePayer payer, FeeCurrency currency) {
        return newOffer(sender, baseOffer.seller, baseOffer.buyer, baseOffer.price.owns, buyeritems,
        gold, payer, currency, 1, OfferStatus.NEGOTIATING, baseOffer)
    }

}
