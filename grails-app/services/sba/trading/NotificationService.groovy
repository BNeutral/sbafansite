package sba.trading

import grails.gorm.transactions.Transactional

@Transactional
class NotificationService {

    def notifyNewAgreement(User toNotify, TradeAgreement agreement) {
        
    }

    def notifyRejectedOffer(User toNotify, Offer rejected) {
        
    }

    def notifySuccessfulAgreement(TradeAgreement agreement) {
        
    }
}
