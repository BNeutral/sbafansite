package sba.trading

import grails.gorm.transactions.Transactional

//Deprecated
@Transactional
class PriceService {
   
    // Selling just one item for gold
    def void newPriceGold (String ownedItemId, int gold) {
        new Price(gold: gold)
            .addToOwns(itemData: ownedItemId, count:1)
            .save(failOnError: true)
    }

    // Selling one or a few of the same item for one or a few of a different item
    def void newPriceSimpleTrade (String ownedItemId, int ownedCount, String wantedItemId, int wantedCount, int gold) {
        new Price(gold: gold)
            .addToOwns(itemData: ownedItemId, count:ownedCount) 
            .addToWants(itemData: wantedItemId, count:wantedCount) 
            .save(failOnError: true)
    }

    // Selling any amount of items for any amount of other items
    def void newPrice (List<String> ownedItemIds, List<Integer> ownedCounts, List<String> wanteditemIds, List<Integer> wantedCounts, int gold) {
        Price price = new Price(gold: gold) 
        for (int i = 0; i < ownedItemIds.size(); ++i) {
            price.addToOwns(itemData: ownedItemIds[i], count:ownedCounts[i]) 
        }
        for (int i = 0; i < wanteditemIds.size(); ++i) {
            price.addToWants(itemData: wanteditemIds[i], count:wantedCounts[i]) 
        }
        price.save(failOnError: true)
    }

    def void remove(Price price) {
        price.delete()
    }
}
