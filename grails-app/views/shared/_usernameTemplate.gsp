<div>
<g:if test="${user == null}">
👤
</g:if>
<g:if test="${user?.reputation == sba.trading.Reputation.SCAMMER}">
💀
</g:if>
<g:if test="${user?.reputation == sba.trading.Reputation.NEW}">
❓
</g:if>
<g:if test="${user?.reputation == sba.trading.Reputation.GOLD}">
🥉
</g:if>
<g:if test="${user?.reputation == sba.trading.Reputation.SILVER}">
🥈
</g:if>
<g:if test="${user?.reputation == sba.trading.Reputation.BRONZE}">
🥇
</g:if>
${user?.username}</div>