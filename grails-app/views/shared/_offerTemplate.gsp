<div class="offer" id="${offer?.id}">
    <table style="width:100%">
        <tr>
            <th>Seller</th>
            <th>Selling</th>
            <th>Wants</th>
            <th>Fees</th>
            <th>Stock</th>
            <th></th>
        </tr>
        <tr>
        <%-- Seller name --%>
            <td width="15%">
                <tmpl:/shared/usernameTemplate user="${offer?.seller}" />
            </td>
        <%-- Selling --%>
            <td width="20%">
                <g:each in="${offer?.price?.owns}" var="itemInstance">
                    <tmpl:/shared/itemInstanceTemplate itemInstance="${itemInstance}" />
                </g:each>
            </td>
        <%-- Buying --%>
            <td width="20%">
                <g:each in="${offer?.price?.wants}" var="itemInstance">
                    <tmpl:/shared/itemInstanceTemplate itemInstance="${itemInstance}" />
                </g:each>
                <g:if test="${offer?.price?.gold > 0}">
                    <div>${offer?.price?.gold} gold</div>
                </g:if>
            </td>
        <%-- Fee --%>
            <td width="20%">
                <div>Fee: <tmpl:/shared/feeTemplate currency="${offer?.currency}" fee="${offer?.fee}" /> </div>
                <div>Paid by: ${offer?.payer}</div>
            </td>
        <%-- Stock --%>
            <td width="10%">${offer?.stock}</td>
        <%-- Buttons --%>
            <td width="10%">
                <button>Buy</button>
                <button>Counteroffer</button>
            </td>
        </tr>
    </table> 
</div>

