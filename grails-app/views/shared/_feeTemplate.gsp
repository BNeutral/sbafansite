<g:if test="${currency == sba.trading.FeeCurrency.ANY}">
    ${fee} GOLD / ${fee*4} iron
</g:if>
<g:if test="${currency == sba.trading.FeeCurrency.IRON}">
    ${fee*4} iron
</g:if>
<g:if test="${currency == sba.trading.FeeCurrency.GOLD}">
    ${fee} gold
</g:if>

