<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Steambirds Alliance Trading Site</title>
</head>
<body>

    <div id="content" role="main">
        <section class="row colset-2-its">
            <h1>Latest Offers</h1>
            <ul>
                <g:findAll in="${offers}" expr="it.seller != session.user">
                    <tmpl:/shared/offerTemplate offer="${it}"/>
                </g:findAll>
            </ul>

        </section>
    </div>

</body>
</html>
