package sba

import sba.trading.*

class BootStrap {

    //def priceService;
    def tradeService;

    def init = { servletContext ->
        initItemData()
        initUsers()
        initOffers()
        initTradings()
        
    }

    def destroy = {
    }

    def initItemData() {
        // TODO: Parse from a text file
        new ItemData("sniper_8", ItemType.GUN, GunType.SNIPER, Tier.T8, 0, 0).save(failOnError: true);
        new ItemData("sniper_9", ItemType.GUN, GunType.SNIPER, Tier.T9, 50, 0.01).save(failOnError: true);
        new ItemData("shotgun_8", ItemType.GUN, GunType.SHOTGUN, Tier.T8, 0, 0).save(failOnError: true);
        new ItemData("armor_8", ItemType.ARMOR, GunType.NONE, Tier.T8, 0, 0).save(failOnError: true);
        new ItemData("armor_10", ItemType.ARMOR, GunType.NONE, Tier.T10, 100, 0.02).save(failOnError: true);
        new ItemData("ion_trail", ItemType.TRAIL, GunType.NONE, Tier.NONE, 1000, 0.1).save(failOnError: true);
        new ItemData("upgrade_armor", ItemType.UPGRADE, GunType.NONE, Tier.NONE, 10, 0.00).save(failOnError: true);
        new ItemData("upgrade_speed", ItemType.UPGRADE, GunType.NONE, Tier.NONE, 10, 0.00).save(failOnError: true);
    }

    def initUsers() {
        def reputations = [Reputation.SCAMMER, Reputation.NEW, Reputation.BRONZE, Reputation.SILVER, Reputation.GOLD]
        for (int i = 0; i <= 20; ++i) {
            String username = "user"+i
            User user = new User(username)
            user.reputation = reputations[i%4]
            if (!user.save(failOnError: true)) {
                log.error "Could not save "+username
                log.error "${user.errors}"
            }            
        }
    }

    /*def initTestPrices() {
        priceService.newPriceGold("sniper_8", 20)
        priceService.newPriceSimpleTrade("sniper_8", 1, "upgrade_speed", 1, 0)
        priceService.newPriceSimpleTrade("sniper_9", 1, "upgrade_armor", 1, 0)
        priceService.newPriceSimpleTrade("armor_10", 1, "upgrade_armor", 2, 0)
        priceService.newPriceGold("armor_10", 150)
        priceService.newPriceGold("ion_trail", 5000)
        priceService.newPrice(["ion_trail"], [1], ["upgrade_armor", "upgrade_speed"], [4, 4], 2000)
        priceService.newPriceGold("upgrade_armor", 100)
        priceService.newPriceGold("upgrade_armor", 80)
        priceService.newPriceGold("upgrade_speed", 40)
        priceService.newPriceGold("upgrade_speed", 35)
        priceService.newPriceSimpleTrade("upgrade_armor", 1, "upgrade_speed", 2, 0)
    }*/

    def initOffers() {
        tradeService.newOfferGold(1, 1, 0, "sniper_8", 20, FeePayer.BUYER, FeeCurrency.ANY, 1)
        tradeService.newOfferSimpleTrade(2, 2, 0, "sniper_8", 1, "upgrade_speed", 1, 0, FeePayer.BUYER, FeeCurrency.ANY, 1)
        tradeService.newOfferSimpleTrade(3, 3, 0, "sniper_9", 1, "upgrade_armor", 1, 0, FeePayer.BUYER, FeeCurrency.ANY, 1)
        tradeService.newOfferSimpleTrade(3, 3, 0, "armor_10", 1, "upgrade_armor", 2, 0, FeePayer.BUYER, FeeCurrency.ANY, 1)
        tradeService.newOfferGold(4, 4, 0, "armor_10", 150, FeePayer.SPLIT, FeeCurrency.IRON, 1)
        tradeService.newOfferGold(4, 4, 0, "ion_trail", 5000, FeePayer.BUYER, FeeCurrency.ANY, 1)
        tradeService.newOffer(3, 3, 0, ["ion_trail"], [1], ["upgrade_armor", "upgrade_speed"], [4, 4], 2000, FeePayer.SPLIT, FeeCurrency.IRON, 10)
        tradeService.newOfferGold(6, 6, 0, "upgrade_armor", 100, FeePayer.BUYER, FeeCurrency.ANY, 10)
        tradeService.newOfferGold(7, 7, 0, "upgrade_armor", 80, FeePayer.SPLIT, FeeCurrency.IRON, 4)
        tradeService.newOfferGold(8, 8, 0, "upgrade_speed", 40, FeePayer.BUYER, FeeCurrency.ANY, 10)
        tradeService.newOfferGold(9, 9, 0, "upgrade_speed", 35, FeePayer.SELLER, FeeCurrency.IRON, 5)
        tradeService.newOfferSimpleTrade(10, 10, 0, "upgrade_armor", 1, "upgrade_speed", 2, 0, FeePayer.SPLIT, FeeCurrency.IRON, 10)
    }

    def initTradings() {
        tradeService.acceptOffer(1, 5)
        //tradeService.newCounterOffer()
        
    }

}
