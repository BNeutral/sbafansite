package sba.trading

class IndexController {

    def index() { 
        def offers = Offer.listOrderByLastUpdated()
        if (session.user != null){
            offers = offers.findAll{ offer -> !(offer.seller.equals(session.user)) && (offer.status == OfferStatus.PUBLIC) }
        }
        render(view:"/index", model: [offers : offers])
    }
}
