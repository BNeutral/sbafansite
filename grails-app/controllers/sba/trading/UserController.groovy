package sba.trading

class UserController {

    static allowedMethods = [profile: "GET", login: ["GET", "POST"], logout: "POST"]

    static defaultAction = "profile"

    // Para otro momento porque no es relevante para el TP
    /*def register = {
        if (request.method == 'POST') {
            // create domain object and assign parameters using data binding
            User user = new User(params)
            if (! user.save()) {
                return [user:user]
            } else {
                session.user = user
                redirect(uri:'/')
            }
        } else if (session.user) {
            redirect(uri:'/')
        }
    }*/

    // Por ahora dejamos esto sin password porque no es relevante para el TP
    def login = {
        if (request.method == 'GET') {
            render(view: "login")
        }
        else if (request.method == 'POST') {
            def user = User.findByUsername(params.username)
            if (user) {
                session.user = user
                redirect(uri:'/')
            } else {
                user = new User(params.username)
                if (!user.save()) {
                    render("Usernames should be between 3 and 32 characters, alphanumeric, and not repeated.")
                }
                else {
                    session.user = user;
                    //flash.message = "Created new user ${params.username}"
                    redirect(uri:'/')
                }
            }
        } else if (session.user) {
            redirect(controller:"user", action:"profile", id: session.user.id)
        }
    }
 
    def profile = {
        if (params.id != null)  {
            def user = User.findById(params.id as int)
            if (user == null) {
                render("Could not find user: "+params.id)
            }
            else {
                render("Found")
            }
        }
        else if (session.user == null) {
            redirect(controller:"user", action:"login")
        }
        else {
            render("Profile for user: ${session.user.username} Id:${session.user.id}")
        }
    }

    def logout = {
        session.invalidate()
        redirect(uri:'/')
    }

}
