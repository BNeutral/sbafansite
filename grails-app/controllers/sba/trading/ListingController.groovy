package sba.trading

class ListingController {

    static defaultAction = "latest"

    def latest() {
        //view: grails-app/views/book/show.gsp for this show action
        //def favoriteBooks = ...
        //return new ModelAndView("/listing/latest", [ bookList : favoriteBooks ])
        render "<h1>Hello World!"
        //render(view: "/index", model: null)
        // render a template for each item in a collection
        // render(template: 'book_template', collection: Book.list())
    }

    def search() {
        def user = session.user //For not listing own items
        def searchType = params.searchType // listing items onSale or onBuy
    }

    def post() {

    }

    def delete() {
        
    }
}
