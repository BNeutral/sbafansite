package sba.trading

import sba.trading.*

import groovy.transform.EqualsAndHashCode
@EqualsAndHashCode
class Price {

    List<ItemInstance> owns
    List<ItemInstance> wants
    int gold

    static hasMany = [owns : ItemInstance, wants: ItemInstance]
    static belongsTo = Offer

    static constraints = {
        owns(nullable: true)
        wants(nullable: true)
        gold(min: 0)
    }

    def int calculateFee() {
        int totalGold = gold
        float percent = 1
        owns.each{
            for (int i = 0; i < it.count; ++i) {
                ItemData data = it.itemData
                totalGold += data.goldCost
                percent += data.goldPercent
            }            
        }
        wants.each{
            for (int i = 0; i < it.count; ++i) {
                ItemData data = it.itemData
                totalGold += data.goldCost
                percent += data.goldPercent
            }            
        }
        return totalGold*percent
    }
}
