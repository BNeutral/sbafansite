package sba.trading

import java.time.LocalDate
import java.util.HashSet

import groovy.transform.EqualsAndHashCode
@EqualsAndHashCode
class User {

    String username
    //String discordUsername
    Reputation reputation = Reputation.BRONZE
    Permissions permissions = Permissions.USER
    boolean reported
    //List<Offer> selling
    LocalDate lastUpdated
    LocalDate dateCreated

    //static hasMany = [selling: Offer]

    static constraints = {
        username(blank: false, nullable: false, size:3..32, matches:/[\w\d]+/, unique:true)
    }

    def User () {
        
    }

    def User (String username) {
        this.username = username
        reported = false
    }

}
