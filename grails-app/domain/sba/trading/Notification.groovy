package sba.trading

import groovy.transform.EqualsAndHashCode
@EqualsAndHashCode
class Notification {

    User owner
    String message
    Offer offer
    TradeAgreement agreement

    static belongsTo = User

    static constraints = {
        owner(nullable: false)
        message(nullable: true)
        offer(nullable: true)
        agreement(nullable: true)
    }
}
