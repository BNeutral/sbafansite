package sba.trading
import java.time.LocalDate

import groovy.transform.EqualsAndHashCode
@EqualsAndHashCode
class TradeAgreement {

    User seller
    User buyer
    User sender
    boolean sellerSuccess = false
    boolean buyerSuccess = false
    Offer offer
    TradeStatus status
    LocalDate lastUpdated

    static constraints = {
        seller(nullable:false)
        buyer(nullable:false)
        offer(nullable:false)
    }

}
