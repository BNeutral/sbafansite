package sba.trading

import groovy.transform.EqualsAndHashCode
@EqualsAndHashCode
class ItemData {

    String id
    ItemType itemType
    GunType gunType
    Tier tier
    int goldCost
    float goldPercent

    static constraints = {
        id(blank: false, nullable: false, unique:true)
    }

    static mapping = {
      id generator: 'assigned',type: 'string'
   }

    public ItemData (String gameId, ItemType itemType, GunType gunType, Tier tier, int goldCost, float goldPercent) {
        this.id = gameId
        this.itemType = itemType
        this.gunType = gunType
        this.tier = tier
        this.goldCost = goldCost
        this.goldPercent = goldPercent
    }
}
