package sba.trading
import java.time.LocalDate

import groovy.transform.EqualsAndHashCode
@EqualsAndHashCode
class Offer {

    User sender
    User seller
    User buyer
    Offer previous
    Price price
    OfferStatus status
    int fee
    FeePayer payer
    FeeCurrency currency
    int stock 
    LocalDate lastUpdated

    static constraints = {
        sender(nullable: false)
        seller(nullable: false)
        buyer(nullable: true)
        previous(nullable: true)
        price(nullable: false)
        fee(min: 0)
        stock(min: 0)
    }

    def reduceStock(){
        if (stock <= 0) return
        stock -= 1
        if (stock <= 0) {
            status = OfferStatus.OUTOFSTOCK
        }
    }

    def getNotSender() {
        if (sender == seller) return buyer
        else return seller
    }
}
