package sba.trading

// Represets an item's id and amount
import groovy.transform.EqualsAndHashCode
@EqualsAndHashCode
class ItemInstance {

    ItemData itemData
    int count

    static belongsTo = Price

    static constraints = {
        itemData(blank: false, nullable: false, unique:false)
        count(min: 1)
    }

    public ItemInstance () {
    }

    public ItemInstance (ItemData data) {
        this.itemData = data
        this.count = 1
    }

    public ItemInstance (ItemData data, int count) {
        this.itemData = data
        this.count = count
    }

    public ItemInstance (String gameID) {
        this.itemData = ItemData.findById(gameID)
        this.count = 1
    }

    public ItemInstance (String gameID, int count) {
        this.itemData = ItemData.findById(gameID)
        this.count = count
    }
}
